#include "WebServer.h"

int main() {
	WebServer server;

	if (server.init(8080)) {
		server.run();
	}

	server.cleanup();

	return 0;
}