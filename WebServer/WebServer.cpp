#include "WebServer.h"
#include <algorithm> //std::transform
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

WebServer::WebServer() {
	m_local.sin_addr.S_un.S_addr = 0;
	m_local.sin_port = 0;
	m_local.sin_family = AF_UNSPEC;
}

WebServer::~WebServer() {
	cleanup();
}

bool WebServer::init(unsigned short p_port) {
	WSAData d;
	WSAStartup(MAKEWORD(2, 1), &d);

	m_local.sin_family = AF_INET;
	//m_local.sin_addr.S_un.S_addr = INADDR_ANY;		//Use to allow external connections
	m_local.sin_addr.S_un.S_un_b.s_b1 = 192;
	m_local.sin_addr.S_un.S_un_b.s_b2 = 168;
	m_local.sin_addr.S_un.S_un_b.s_b3 = 1;
	m_local.sin_addr.S_un.S_un_b.s_b4 = 20;
	m_local.sin_port = htons(p_port);
	memset(m_local.sin_zero, 0, sizeof(m_local.sin_zero));

	if (createAndBindSocket((const struct sockaddr*)&m_local) < 0) {
		return false;
	}

	return true;
}

void WebServer::run() {
	bool running = true;
	struct fd_set set;
	set.fd_count = 1;
	set.fd_array[0] = m_listenerSocket;

	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 10;

	while (running) {
		for (unsigned int i = 0; i < m_connectedClients.size();) {
			if (timeGetTime() - m_connectedClients[i]->lastRequest > 10000) {
				closesocket(m_connectedClients[i]->socket);
				m_connectedClients.erase(m_connectedClients.begin() + i);
			}
			else {
				handleData(receiveData(&m_connectedClients[i]->socket), *m_connectedClients[i]);
				i++;
			}
		}

		int sel = select(1, &set, 0, 0, &tv);
		if (sel > 0) {
			Client client;
			client.lastRequest = timeGetTime();

			client.addr.sin_family = AF_INET;
			client.addr.sin_addr.S_un.S_addr = 0;
			client.addr.sin_port = htons(8080);
			memset(client.addr.sin_zero, 0, sizeof(client.addr.sin_zero));

			int size = sizeof(struct sockaddr_in);

			client.socket = accept(m_listenerSocket, (sockaddr*)&client.addr, &size);

			if (client.socket == INVALID_SOCKET) {
				std::cout << "Run() -> select(): Couldn't create socket! Err: " << WSAGetLastError() << std::endl;
			}

			std::cout << static_cast<int>(client.addr.sin_addr.S_un.S_un_b.s_b1) << "."
				<< static_cast<int>(client.addr.sin_addr.S_un.S_un_b.s_b2) << "."
				<< static_cast<int>(client.addr.sin_addr.S_un.S_un_b.s_b3) << "."
				<< static_cast<int>(client.addr.sin_addr.S_un.S_un_b.s_b4) << std::endl;

			handleData(receiveData(&client.socket), client);
			m_connectedClients.push_back(&client);
		}
		else if (sel == SOCKET_ERROR) {
			std::cout << "Select error: " << WSAGetLastError() << std::endl;
		}

		set.fd_count = 1;			//Need to set the socket to the set again, since select removes it if there's no data
		set.fd_array[0] = m_listenerSocket;

		if (GetAsyncKeyState('q') != 0 || GetAsyncKeyState('Q') != 0) {
			running = false;
		}

		::Sleep(10);		//Lets not kill our CPU, shall we?
	}
}

void WebServer::cleanup() {
	closesocket(m_listenerSocket);

	auto it = m_connectedClients.begin();
	while (it != m_connectedClients.end()) {
		closesocket((*it)->socket);
		it = m_connectedClients.erase(it);
	}

	WSACleanup();
}

int WebServer::createAndBindSocket(const struct sockaddr *p_addr) {
	m_listenerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_listenerSocket == INVALID_SOCKET) {
		std::cout << "Error: " << WSAGetLastError() << std::endl;
		return -1;
	}

	if (bind(m_listenerSocket, p_addr, sizeof(struct sockaddr)) < 0) {
		std::cout << "Bind error: " << WSAGetLastError() << std::endl;
		return -1;
	}

	if (listen(m_listenerSocket, SOMAXCONN) < 0) {
		std::cout << "Listen error: " << WSAGetLastError() << std::endl;
		return -1;
	}

	return 0;
}

std::string WebServer::receiveData(SOCKET *p_socket) {
	u_long len = 0;
	if (ioctlsocket(*p_socket, FIONREAD, &len) == SOCKET_ERROR) {
		std::cout << "Ioctlsocket error: " << WSAGetLastError() << std::endl;
		return "";
	}

	std::string temp = "";

	while (len > 0) {
		char data[4096];

		int bytes = recv(*p_socket, data, sizeof(data) - 1, 0);
		if (bytes < 0) {
			std::cout << "Socket error - receiveData: " << WSAGetLastError() << std::endl;
			return 0;
		}

		data[bytes] = '\0';
		temp = data;

		if (ioctlsocket(*p_socket, FIONREAD, &len) == SOCKET_ERROR) {
			std::cout << "Ioctlsocket error: " << WSAGetLastError() << std::endl;
			return "";
		}
	}

	return temp;
}

void WebServer::handleData(const std::string &p_data, struct Client &p_client) {
	if (p_data.size() <= 0) {
		return;		//nothing to do
	}
	else if (p_data.find("GET ") == 0) {
		if (p_data.find("\\..\\") != p_data.npos || p_data.find("/../") != p_data.npos) {		//First one might be redundant, dunno.
			p_client.lastRequest = 0;		//Close connection with people trying to exploit
			return;
		}

		p_client.lastRequest = timeGetTime();

		std::cout << p_data << std::endl;

		if (p_data.find("Connection:") != p_data.npos) {
			//Create a substring from "Connection" to the first "\r\n" on that line, then handle that line
			handleConnectionData(p_data.substr(p_data.find("Connection:"), (p_data.find("Connection:") - p_data.find("\r\n", p_data.find("Connection:")))), p_client);
		}

		sendRequestedData(p_data.substr(0, p_data.find("\r\n")), p_client);		//I don't really like sending the address all this way, but meh.
	}
}

void WebServer::handleConnectionData(std::string p_data, struct Client &p_client) {
	std::transform(p_data.begin(), p_data.end(), p_data.begin(), ::tolower);	//http://notfaq.wordpress.com/2007/08/04/cc-convert-string-to-upperlower-case/

	if (p_data.find("close") != p_data.npos) {		//Can add more cases
		auto it = m_connectedClients.begin();
		while (it != m_connectedClients.end()) {
			if ((*it) == &p_client) {
				m_connectedClients.erase(it);
				break;
			}
		}
	}
}

void WebServer::sendRequestedData(const std::string &p_data, const struct Client &p_client) {
	std::string sendData = "", requestedPath;

	requestedPath = p_data.substr(p_data.find("/"), (p_data.find("HTTP") - 1) - p_data.find("/"));		//The ghetto fixes are real

	sendData += p_data.substr(p_data.find("HTTP"));
	sendData += constructSendData(requestedPath);
	
	int bytes = sendto(p_client.socket, sendData.c_str(), sendData.size(), 0, (sockaddr*)&p_client.addr, sizeof(sockaddr));

	if (bytes == SOCKET_ERROR) {
		std::cout << "Socket error - send: " << WSAGetLastError() << std::endl;
	}
}

std::string WebServer::constructSendData(std::string &p_requestedPath) {
	std::string sendData = "";
	std::ifstream stream;

	if (p_requestedPath[0] == '/') {
		p_requestedPath.erase(0, 1);
	}

	if (p_requestedPath.find(".") == p_requestedPath.npos) {		//If not requesting a file with a specific ending, default to index.html in that folder
		stream.open(p_requestedPath + "index.html");
	}
	else {
		stream.open(p_requestedPath, std::fstream::binary);
	}

	if (stream.is_open()) {
		//Thanks to http://stackoverflow.com/a/2602258 for this solution
		std::stringstream buffer;
		buffer << stream.rdbuf();

		sendData += constructHeader(200, buffer.str().size());
		sendData += buffer.str();

		buffer.clear();
		stream.close();
	}
	else {
		std::string response = "<HTML>\r\n<BODY>\r\n<CENTER><h1>404 Not Found</h1></CENTER>\r\n</BODY>\r\n</HTML>\r\n";
		sendData += constructHeader(404, response.length());
		sendData += response;
	}

	return sendData;
}

std::string WebServer::constructHeader(const unsigned short &p_code, const unsigned int &p_length) {
	std::string retval = "";

	switch (p_code) {		//Can add more error codes if wanted/needed
	case 200: {
		retval += " 200 OK\r\n";
		break;
	}
		
	case 404: {
		retval += " 404 Not Found\r\n";
		break;
	}

	default: {
		break;
	}
	}

	std::stringstream sstream;
	sstream << "Connection: keep-alive\r\n"
			<< "Server: Fisk-Server\r\n"
			<< "Keep-Alive: timeout=10000, max=15000\r\n"
			<< "Content-Type: text/html\r\n"
			<< "Content-Length: " << std::to_string(p_length) << "\r\n";
	
	retval += sstream.str();
	retval += "\r\n";

	return retval;
}