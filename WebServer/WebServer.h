#include <string>
#include <vector>
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "Winmm.lib")	//Allow timeGetTime()

class WebServer {
public:
	WebServer();
	~WebServer();

	bool init(unsigned short p_port);
	void run();
	void cleanup();
private:
	struct Client {
		sockaddr_in addr;
		SOCKET socket;
		unsigned int lastRequest;
	};

	int createAndBindSocket(const struct sockaddr *p_addr);
	std::string receiveData(SOCKET *p_socket);
	void handleData(const std::string &p_data, struct Client &p_client);

	void handleConnectionData(std::string p_data, struct Client &p_client);

	void sendRequestedData(const std::string &p_data, const struct Client &p_addr);
	std::string constructSendData(std::string &p_requestPath);
	std::string constructHeader(const unsigned short &p_code, const unsigned int &p_messageLength);		//What code to return

	struct sockaddr_in m_local;
	SOCKET m_listenerSocket;

	std::vector<Client*> m_connectedClients;
};
